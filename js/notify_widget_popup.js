(function ($, Drupal, once) {

  Drupal.behaviors.notify_widget = {
    attach: function attach(context) {

      // Add click event listener to the notifications popup element.
      once('notify_widget', '#notify_toggle', context).forEach(element => {
        element.addEventListener('click', e => {
          e.preventDefault();
          e.stopPropagation();
          if (!$('#notify_widget_popup').hasClass('is-open')) {
            $('#notify_widget_popup').addClass('is-open');
          }
          else {
            $('#notify_widget_popup').removeClass('is-open');
          }
        });
      });

      // Add a click listener to the body to close the notifications popup.
      once('notify_widget', 'body').forEach(element => {
        element.addEventListener('click', e => {
          $('#notify_widget_popup').removeClass('is-open');
        });
      });

      // Add a click listener to the popup, so we can stop propagation of
      // events so clicking the popup doesn't close itself by allowing the
      // event to reach to body element.
      once('notify_widget', '#notify_widget_popup', context).forEach(element => {
        element.addEventListener('click', e => {
          e.stopPropagation();
        });
      });
    }
  };

})(jQuery, Drupal, once);
