require 'compass/import-once/activate'
require 'sass-globbing'
http_path = "/"
css_dir = "./css"
sass_dir = "./sass"
images_dir = "./images"
javascripts_dir = "./js"
output_style = :compressed
