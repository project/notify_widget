<?php declare(strict_types = 1);

namespace Drupal\notify_widget\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\notify_widget\NotifyWidgetApi;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a system action for bulk notification sending.
 */
final class NotifyWidgetSendActionConfirmForm extends ConfirmFormBase {

  /**
   * The temp store factory.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStoreFactory
   */
  protected $tempStoreFactory;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The notify widget API service.
   *
   * @var \Drupal\notify_widget\NotifyWidgetApi
   */
  protected $notifyWidgetApi;

  /**
   * Constructs a new UserMultipleCancelConfirm.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The temp store factory.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   The user storage.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\notify_widget\NotifyWidgetApi $notifyWidgetApi
   *   The notify widget API service.
   */
  public function __construct(
    PrivateTempStoreFactory $temp_store_factory,
    UserStorageInterface $user_storage,
    EntityTypeManagerInterface $entity_type_manager,
    NotifyWidgetApi $notifyWidgetApi
  ) {
    $this->tempStoreFactory = $temp_store_factory;
    $this->userStorage = $user_storage;
    $this->entityTypeManager = $entity_type_manager;
    $this->notifyWidgetApi = $notifyWidgetApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager')->getStorage('user'),
      $container->get('entity_type.manager'),
      $container->get('notify_widget.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'notify_widget_notify_widget_send_action_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Send message to the selected user(s)');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return 'Enter the message details below...';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.user.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Send');
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Retrieve the accounts to send notifications to from the temp store.
    /** @var \Drupal\user\Entity\User[] $accounts */
    $accounts = $this->tempStoreFactory
      ->get('notify_widget_send_action')
      ->get($this->currentUser()->id());
    if (!$accounts) {
      return $this->redirect('entity.user.collection');
    }

    $names = [];
    $form['accounts'] = ['#tree' => TRUE];
    foreach ($accounts as $account) {
      $uid = $account->id();
      $names[$uid] = $account->label();

      $form['accounts'][$uid] = [
        '#type' => 'hidden',
        '#value' => $uid,
      ];
    }

    $form['account']['names'] = [
      '#theme' => 'item_list',
      '#items' => $names,
    ];

    $form = parent::buildForm($form, $form_state);

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notification Title'),
      '#description' => $this->t('Enter the text that will form the title of the notification.'),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Notification Text'),
      '#description' => $this->t('Enter the text that will form the body of the notification.'),
    ];

    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Notification Type'),
      '#description' => $this->t('Select the notification type.'),
      '#options' => [
        'add' => $this->t('Information'),
        'alert' => $this->t('Warning'),
      ],
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notification Destination'),
      '#description' => $this->t('Enter a URL that the notification will click through to.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();
    $current_user_id = $this->currentUser()->id();

    // Clear out the accounts from the temp store.
    $this->tempStoreFactory->get('notify_widget_send_action')->delete($current_user_id);

    foreach ($values['accounts'] as $uid => $value) {
      $this->notifyWidgetApi->send(
        'bulk',
        $values['type'],
        $values['title'],
        $values['message'],
        $uid,
        $values['url']
      );
    }

    $this->messenger()->addStatus($this->t('Notifications sent.'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
