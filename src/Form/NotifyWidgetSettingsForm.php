<?php declare(strict_types = 1);

namespace Drupal\notify_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Notify Widget settings for this site.
 */
final class NotifyWidgetSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'notify_widget_notify_widget_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['notify_widget.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['max_notifications'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum Notifications to Display'),
      '#description' => $this->t('Select the maximum number of notifications to display in the popup overlay.'),
      '#default_value' => $this->config('notify_widget.settings')->get('max_notifications') ?? 10,
      '#required' => TRUE,
      '#min' => 1,
      '#max' => 5000,
      '#step' => 1,
    ];

    $form['include_read'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Read Notifications'),
      '#description' => $this->t('Check this box to include read notifications in the notifications pop-up.'),
      '#default_value' => $this->config('notify_widget.settings')->get('include_read') ?? TRUE,
    ];

    $form['read_cutoff'] =[
      '#type' => 'select',
      '#title' => $this->t('Read Cut-Off'),
      '#description' => $this->t('Select a value from the list. Any read notifications that fall outside of the selected period will not show in the notifications pop-up.'),
      '#options' => [
        '0' => $this->t('No cutoff'),
        '900' => $this->t('15 minutes'),
        '1800' => $this->t('30 minutes'),
        '3600' => $this->t('1 hour'),
        '21600' => $this->t('6 hours'),
        '86400' => $this->t('1 day'),
        '604800' => $this->t('1 week'),
      ],
      '#default_value' => $this->config('notify_widget.settings')->get('read_cutoff') ?? 0,
    ];

    $form['use_module_css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Module CSS'),
      '#description' => $this->t('Check this box to use the CSS included with the module. If you want to use your own CSS, uncheck this and target the #notify_widget element with your own CSS.'),
      '#default_value' => $this->config('notify_widget.settings')->get('use_module_css') ?? 1,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('notify_widget.settings')
      ->set('max_notifications', $form_state->getValue('max_notifications'))
      ->set('use_module_css', $form_state->getValue('use_module_css'))
      ->set('include_read', $form_state->getValue('include_read'))
      ->set('read_cutoff', $form_state->getValue('read_cutoff'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
