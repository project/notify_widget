<?php declare(strict_types = 1);

namespace Drupal\notify_widget\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\Entity\User;

/**
 * Checks user parameter matches the logged in user.
 *
 * Usage example:
 * @code
 * foo.example:
 *   path: '/example/{parameter}'
 *   defaults:
 *     _title: 'Example'
 *     _controller: '\Drupal\notify_widget\Controller\NotifyWidgetController'
 *   requirements:
 *     _user_can_access_notifications: 'some value'
 * @endcode
 */
final class NotificationPageAccessChecker implements AccessInterface {

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   */
  public function __construct(
    protected readonly AccountProxyInterface $currentUser
  ) {}

  /**
   * Access callback.
   *
   * Only allow access to the page is the user object in the route parameter
   * matches that of the currently logged-in user.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user object from the route parameters.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The result of the access check.
   */
  public function access(User $user): AccessResult {
    return AccessResult::allowedIf($user->id() === $this->currentUser->id());
  }

}
