<?php declare(strict_types = 1);

namespace Drupal\notify_widget\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Connection;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\Entity\User;

/**
 * Checks if the user in the route parameter matches the owner of the
 * notification ID in the route parameter.
 *
 * Usage example:
 * @code
 * foo.example:
 *   path: '/example/{parameter}'
 *   defaults:
 *     _title: 'Example'
 *     _controller: '\Drupal\notify_widget\Controller\NotifyWidgetController'
 *   requirements:
 *     _user_owns_notification: 'some value'
 * @endcode
 */
final class UserOwnsNotificationAccessChecker implements AccessInterface {

  /**
   * Constructs an UserOwnsNotificationAccessChecker object.
   */
  public function __construct(
    private readonly Connection $database,
    private readonly AccountProxyInterface $currentUser,
  ) {}

  /**
   * Access callback.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user from the route parameters.
   * @param int $notificationId
   *   The notification ID from the route parameters.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The result of the access check.
   */
  public function access(User $user, int $notificationId): AccessResult {
    $result = $this->database->select('notify_widget', 'n')
      ->fields('n', ['id'])
      ->condition('id', $notificationId)
      ->condition('uid', $this->currentUser->id())
      ->range(0, 1)
      ->execute()
      ->fetchField();
    if ($result) {
      return AccessResult::allowed();
    }
    else {
      return AccessResult::forbidden();
    }
  }

}
