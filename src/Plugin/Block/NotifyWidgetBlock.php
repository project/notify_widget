<?php

namespace Drupal\notify_widget\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Notify Widget' block.
 *
 * @Block(
 *  id = "notify_widget_block",
 *  admin_label = @Translation("Notify Widget"),
 * )
 */
class NotifyWidgetBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Lazy build the block contents for maximum cache-goodness.
    $output['notify_widget_block'] = [
      '#lazy_builder' => [
        'notify_widget.lazy_builders:renderNotifyWidgetBlock',
        [],
      ],
      '#create_placeholder' => TRUE,
    ];

    return $output;
  }

}
