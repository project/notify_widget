<?php declare(strict_types = 1);

namespace Drupal\notify_widget;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Provides an API for sending notifications to users.
 */
final class NotifyWidgetApi {

  /**
   * The notify widget module config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a NotifyWidgetApi object.
   */
  public function __construct(
    protected readonly Connection $database,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly AccountProxyInterface $currentUser,
    protected readonly CacheTagsInvalidatorInterface $cacheTagsInvalidator,
    protected readonly TimeInterface $time
  ) {
    $this->config = $this->configFactory->get('notify_widget.settings');
  }

  /**
   * Send a notification to a user.
   *
   * @param string $source
   *   The notification source.
   * @param string $type
   *   The notification type.
   * @param string $title
   *   The notification title.
   * @param string $text
   *   The notification text.
   * @param int|array $uid
   *   The recipient user ID or an array of user IDs.
   * @param string $link
   *   The link the notification should take the user to.
   *
   * @throws \Exception
   */
  public function send(
    string $source,
    string $type,
    string $title,
    string $text,
    int|array $uid = 0,
    string $link = ''
  ): void {
    // If no user ID has been specified, assume the current user ID.
    if ($uid === 0) {
      $uid = $this->currentUser->id();
    }

    // Insert the details into the notify_widget database table.
    $query = $this->database->insert('notify_widget')
      ->fields([
        'uid',
        'source',
        'read',
        'notification_type',
        'notification_title',
        'notification_text',
        'link',
        'timestamp',
      ]);

    if (is_array($uid)) {
      $values = [];
      foreach ($uid as $id) {
        $values[] = [
          'uid' => $id,
          'source' => $source,
          'read' => 0,
          'notification_type' => $type,
          'notification_title' => $title,
          'notification_text' => $text,
          'link' => $link,
          'timestamp' => $this->time->getRequestTime(),
        ];
      }
    }
    else {
      $values[] = [
        'uid' => $uid,
        'source' => $source,
        'read' => 0,
        'notification_type' => $type,
        'notification_title' => $title,
        'notification_text' => $text,
        'link' => $link,
        'timestamp' => $this->time->getRequestTime(),
      ];
    }

    if (is_array($values)) {
      foreach ($values as $value) {
        $query->values($value)->execute();
      }
    }
    else {
      $query->values($values);
    }

    // Invalidate the notify widget cache for the current user.
    $this->clearNotifyWidgetCacheForUser($uid);
  }

  /**
   * Mark a notification as read.
   *
   * @param int $id
   *   The notification ID to set as read.
   */
  public function markAsRead(int $id): void  {
    $this->setNotificationStatus($id, 1);
  }

  /**
   * Mark a notification as unread.
   *
   * @param int $id
   *   The notification ID to set as unread.
   */
  public function markAsUnread(int $id): void  {
    $this->setNotificationStatus($id, 0);
  }

  /**
   * Set notification status.
   *
   * @param int $id
   *   The ID of the notification.
   * @param int $status
   *   The status to set the notification to. 0 = unread, 1 = read.
   */
  private function setNotificationStatus(int $id, int $status): void {
    $this->database->update('notify_widget')
      ->fields([
        'read' => $status,
      ])
      ->condition('id', $id)
      ->execute();

    // Invalidate the notify widget cache for the current user.
    $this->clearNotifyWidgetCacheForUser();
  }

  /**
   * Get the notifications for a user.
   *
   * @param int $uid
   *   The User ID to fetch notifications for. Defaults to the current user.
   * @param int $id
   *   ID of a specific notification to fetch. All notifications are
   *   returned for the current user if no ID is supplied.
   * @param bool $includeRead
   *   Set TRUE to include read notifications.
   * @param int $readCutoff
   *   Timestamp for which read notifications will only be shown
   *   if they are younger than the timestamp. If $includeRead is FALSE
   *   this has no effect, and if $includeRead is TRUE and this is 0 (zero)
   *   then no cut-off is applied.
   *
   * @return array
   *   An array of stdClass objects, where each object is
   *   a DB row (notification).
   */
  public function getNotificationsForUser(int $uid = 0, int $id = 0, bool $includeRead = TRUE, int $readCutoff = 0): array {
    // Get the maximum number of notifications to show in the widget.
    $maxNotifications = $this->config->get('max_notifications') ?? 10;

    if ($uid === 0) {
      $uid = $this->currentUser->id();
    }

    // Get the notification details for the user.
    $query = $this->database->select('notify_widget', 'n')
      ->fields('n', [])
      ->condition('uid', $uid)
      ->orderBy('timestamp', 'desc')
      ->range(0, $maxNotifications);

    // If a notification ID has been passed in, add it to the query.
    if ($id <> 0) {
      $query->condition('id', $id);
    }

    // Make sure we only fetch unread or both read and unread.
    if (!$includeRead) {
      $query->condition('read', 0);
    }
    elseif ($includeRead && $readCutoff !== 0) {
      // Only include read notifications if their timestamp is
      // within the cut-off period.
      $andGroup = $query->andConditionGroup()
        ->condition('read', 1)
        ->condition('timestamp', $readCutoff, '>');
      $orGroup = $query->orConditionGroup()
        ->condition('read', 0)
        ->condition($andGroup);
      $query->condition($orGroup);
    }

    return $query->execute()->fetchAll();
  }

  /**
   * Get the number of unread notifications for a user.
   *
   * @return int
   *   The number of unread notifications.
   */
  public function getUnreadNotificationsCount(): int {
    // Get a count of the total number of unread notifications.
    $unreadCountQuery = $this->database->select('notify_widget', 'n')
      ->fields('n', ['id'])
      ->condition('uid', $this->currentUser->id())
      ->condition('read', 0);
    return (int) $unreadCountQuery->countQuery()->execute()->fetchField() ?? 0;
  }

  /**
   * Clear the notify widget cache for the logged in user.
   */
  public function clearNotifyWidgetCacheForUser($uid = 0): void {
    if ($uid === 0) {
      $uid = $this->currentUser->id();
    }
    // Invalidate the notify widget cache for the current user.
    if (is_array($uid)) {
      foreach ($uid as $id) {
        $this->cacheTagsInvalidator->invalidateTags(['notify_widget:' . $id]);
      }
    }
    else {
      $this->cacheTagsInvalidator->invalidateTags(['notify_widget:' . $uid]);
    }
  }

  /**
   * Delete notification.
   *
   * @param int $notificationId
   *   The ID of the notification to delete.
   */
  public function deleteNotification(int $notificationId) {
    $this->database->delete('notify_widget')
      ->condition('id', $notificationId)
      ->execute();
  }

}
