<?php declare(strict_types = 1);

namespace Drupal\notify_widget\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\notify_widget\NotifyWidgetApi;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for Notify Widget routes.
 */
final class NotificationsController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    protected readonly NotifyWidgetApi $notifyWidgetApi,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('notify_widget.api'),
    );
  }

  /**
   * Builds the response.
   *
   * @param \Drupal\user\Entity\User|null $user
   *   The user object.
   */
  public function __invoke(User $user = NULL): array {
    // Get the notifications for the user.
    $notifications = $this->notifyWidgetApi->getNotificationsForUser((int) $user->id());

    $header = [
      ['data' => $this->t('Date/Time')],
      ['data' => $this->t('Notification Type')],
      ['data' => $this->t('Title')],
      ['data' => $this->t('Text')],
      ['data' => $this->t('Read')],
      ['data' => $this->t('Actions')],
    ];

    $rows = [];
    foreach ($notifications as $notification) {
      
      try {
        $url = Url::fromUserInput($notification->link);
      }
      catch (\Exception $e) {
        $url = Url::fromUserInput('/');
      }

      $rows[$notification->id] = [
        'data' => [
          ['data' => date('d/m/Y H:i', (int) $notification->timestamp)],
          ['data' => $notification->notification_type],
          ['data' => $notification->notification_title],
          ['data' => $notification->notification_text],
          ['data' => $notification->read ? $this->t('Yes') : $this->t('No')],
          [
            'data' => [
              '#type' => 'dropbutton',
              '#links' => [
                'view' => [
                  'title' => $this->t('Go to link'),
                  'url' => $url,
                ],
                'toggle' => [
                  'title' => $notification->read ? $this->t('Mark as unread') : $this->t('Mark as read'),
                  'url' => $notification->read
                    ? Url::fromRoute('notify_widget.notifications.mark_as_unread', [
                      'user' => $this->currentUser()->id(),
                      'notificationId' => $notification->id,
                    ],
                      [
                        'query' => $this->getDestinationArray(),
                      ])
                    : Url::fromRoute('notify_widget.notifications.mark_as_read', [
                      'user' => $this->currentUser()->id(),
                      'notificationId' => $notification->id,
                    ],
                      [
                        'query' => $this->getDestinationArray(),
                      ]),
                ],
                'delete' => [
                  'title' => $this->t('Delete'),
                  'url' => Url::fromRoute('notify_widget.delete_notification_confirm', [
                    'user' => $this->currentUser()->id(),
                    'notificationId' => $notification->id,
                  ],
                    [
                      'query' => $this->getDestinationArray(),
                    ]),
                ],
              ],
            ],
          ],
        ],
        'class' => $notification->read ? ['read'] : [],
      ];
    }

    $build['notifications'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('You have no notifications.'),
    ];

    // Cache the block by user ID.
    $build['#cache'] = [
      'tags' => ['notify_widget:' . $this->currentUser()->id()],
    ];

    $build['#attached']['library'][] = 'notify_widget/dropbutton';

    return $build;
  }

  /**
   * Mark a notification as read.
   *
   * @param int $notificationId
   *   The ID of the notification to mark as read.
   */
  public function markRead(int $notificationId) {
    $this->notifyWidgetApi->markAsRead($notificationId);
    return new RedirectResponse(\Drupal::destination()->get());
  }

  /**
   * Mark a notification as unread.
   *
   * @param int $notificationId
   *   The ID of the notification to mark as unread.
   */
  public function markUnread(int $notificationId) {
    $this->notifyWidgetApi->markAsUnread($notificationId);
    return new RedirectResponse(\Drupal::destination()->get());
  }

}
