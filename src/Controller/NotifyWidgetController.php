<?php declare(strict_types = 1);

namespace Drupal\notify_widget\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\notify_widget\NotifyWidgetApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Notify Widget routes.
 */
class NotifyWidgetController extends ControllerBase {

  /**
   * The notify widget API service.
   *
   * @var \Drupal\notify_widget\NotifyWidgetApi
   */
  protected $notifyWidgetApi;

  /**
   * The controller constructor.
   */
  public function __construct(
    NotifyWidgetApi $notifyWidgetApi
  ) {
    $this->notifyWidgetApi = $notifyWidgetApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('notify_widget.api')
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(int $id = 0) {
    // Get the notification from the database (crossed referenced with
    // the current user ID to ensure only the user that the notification
    // was sent to can access it.
    $notification = $this->notifyWidgetApi->getNotificationsForUser(0, $id);

    if ($notification) {
      $this->notifyWidgetApi->markAsRead($id);
      return new RedirectResponse($notification[0]->link);
    }
    else {
      throw new NotFoundHttpException();
    }

    return [];
  }

}
