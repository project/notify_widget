<?php declare(strict_types = 1);

namespace Drupal\notify_widget;

use DateTime;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserData;

/**
 * Defines a service for comment #lazy_builder callbacks.
 */
class NotifyWidgetLazyBuilders implements TrustedCallbackInterface {

  /**
   * Constructs a new NotifyWidgetLazyBuilders object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\notify_widget\NotifyWidgetApi $notifyWidgetApi
   *   The notify widget API service.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Component\DateTime\TimeInterface $time
   *   The time service.
   */
  public function __construct(
    protected readonly Connection $database,
    protected readonly NotifyWidgetApi $notifyWidgetApi,
    protected readonly AccountProxyInterface $currentUser,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly TimeInterface $time,
    protected readonly UserData $userData
  ) {}

  /**
   * Lazy builder callback; builds the Notify Widget block.
   *
   * @return array
   *   A render array containing the block content.
   */
  public function renderNotifyWidgetBlock(): array {
    // Get the config settings.
    $includeRead = (bool) $this->configFactory->get('notify_widget.settings')->get('include_read') ?? TRUE;
    $readCutoff = (int) $this->configFactory->get('notify_widget.settings')->get('read_cutoff') ?? 0;

    // If readCutoff is not zero, take the current timestamp and subtract
    // the selected cutoff from it.
    if ($readCutoff !== 0) {
      $readCutoff = $this->time->getRequestTime() - $readCutoff;
    }

    // Get the notification details for the user.
    $notifications = $this->notifyWidgetApi->getNotificationsForUser(0,0, $includeRead, $readCutoff);
    $unreadCount = $this->notifyWidgetApi->getUnreadNotificationsCount();

    // If unread count is more than 20, set the count to 20+.
    if ($unreadCount > 20) {
      $unreadCount = '20+';
    }

    // Convert the timestamp for each notification to 'time ago'.
    foreach ($notifications as $key => $notification) {
      $notification->timestamp = $this->relativeTime($notification->timestamp);
    }

    // Get the current user's ID and the flag indicating whether they are
    // new to the notifications system.
    $uid = $this->currentUser->id();
    $newToNotifications = empty($this->userData->get('notify_widget', $uid, 'new_to_notifications'))
      ? 'yes' :
      $this->userData->get('notify_widget', $uid, 'new_to_notifications');

    // Build the render array for the block.
    $build = [];
    $build['#theme'] = 'notify_widget_block';
    $build['#notifications'] = $notifications;
    $build['#unread_count'] = $unreadCount;
    $build['#uid'] = $uid;
    $build['#new_to_notifications'] = !($newToNotifications === 'no');

    // If the user had not seen the notifications block prior to this build
    // they now have, so we can set a flag in their userdata to indicate this.
    // We only set this flag if there are no notifications to show.
    if ($newToNotifications && !count($notifications)) {
      $this->userData->set('notify_widget', $uid, 'new_to_notifications', 'no');
    }

    // Attach JS library for pop-up, if configured to use module CSS.
    if ($this->configFactory->get('notify_widget.settings')->get('use_module_css') ?? 1) {
      $build['#attached']['library'][] = 'notify_widget/notifications_popup_css';
    }
    else {
      $build['#attached']['library'][] = 'notify_widget/notifications_popup';
    }

    // Cache the block by user ID.
    $build['#cache'] = [
      'tags' => ['notify_widget:' . $this->currentUser->id()],
    ];

    return $build;
  }

  /**
   * Convert a timestamp to time-ago format.
   *
   * @param int|string $ts
   *   The timestamp to convert.
   *
   * @return string
   */
  private function relativeTime(int|string $ts): string {
    if (!ctype_digit($ts))
      $ts = strtotime($ts);

    $diff = time() - $ts;
    if ($diff == 0) {
      return 'now';
    }
    elseif ($diff > 0) {
      $day_diff = floor($diff / 86400);
      if ($day_diff == 0) {
        if ($diff < 60) return 'just now';
        if ($diff < 120) return '1 minute ago';
        if ($diff < 3600) return floor($diff / 60) . ' minutes ago';
        if ($diff < 7200) return '1 hour ago';
        if ($diff < 86400) return floor($diff / 3600) . ' hours ago';
      }
      if ($day_diff == 1) return 'Yesterday';
      if ($day_diff < 7) return $day_diff . ' days ago';
      if ($day_diff < 14) return '1 week ago';
      if ($day_diff < 31) return ceil($day_diff / 7) . ' weeks ago';
      if ($day_diff < 60) return 'last month';
      return date('F Y', (int) $ts);
    }
    else {
      $diff = abs($diff);
      $day_diff = floor($diff / 86400);
      if ($day_diff == 0) {
        if ($diff < 120) return 'in a minute';
        if ($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
        if ($diff < 7200) return 'in an hour';
        if ($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
      }
      if ($day_diff == 1) return 'Tomorrow';
      if ($day_diff < 4) return date('l', $ts);
      if ($day_diff < 7 + (7 - date('w'))) return 'next week';
      if (ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
      if (date('n', $ts) == date('n') + 1) return 'next month';
      return date('F Y', $ts);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['renderNotifyWidgetBlock'];
  }

}
